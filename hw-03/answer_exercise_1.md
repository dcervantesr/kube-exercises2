# Resolución
- Cree un fichero namespase
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: nginx-server
```

- Cree un fichero deployment
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-server
  namespace: nginx-server
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app:  nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4
        resources:
          limits:
            memory: "128Mi"
            cpu: "20m"
          requests:
            memory: "128Mi"
            cpu: "20m"
        ports:
        - containerPort: 80
```

- Cree un fichero service
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  namespace: nginx-server
  labels:
    app: nginx-server
spec:
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  type: ClusterIP
```

- Cree un fichero ingress
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-server
  namespace: nginx-server
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: damian.student.lasalle.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nginx-server
            port:
              number: 80
```

- Ejecuté los comandos:
```shell
1. minikube start
2. minikube addons enable ingress
3. mini kube addons enable ingress-dns
4. kubectl apply -f namespase.yaml
5. kubectl apply -f deployment.yaml
6. kubectl apply -f service.yaml
7. kubectl apply -f ingress.yaml
```

- Modifique el fichero hosts (esto es por el Mac M1 que no puedo usar el IP de minikube)
127.0.0.1 damian.student.lasalle.com

- Ejecuté el comando:
```shell
1. minikube tunnel
```

- Accedí al sitio web http://damian.student.lasalle.com
![](../imagenes/img1.png)

- Instale OpenSSL
```shell
brew install openssl
```

- Generé la ingress-tls
```shell
openssl req -x509 -nodes -days 30 -newkey rsa:2048 -keyout ingress-tls.key -out ingress-tls.crt -subj "/CN=damian.student.lasalle.com/O=ingress-tls"
```

-- Cree un fichero secret
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: ingress-tls
  namespace: nginx-server
data:
  tls.crt: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tDQpNSUlDOGpDQ0Fkb0NDUUNaRGJSRCtXdGEyVEFOQmdrcWhraUc5dzBCQVFzRkFEQTdNU013SVFZRFZRUUREQnBrDQpZVzFwWVc0dWMzUjFaR1Z1ZEM1c1lYTmhiR3hsTG1OdmJURVVNQklHQTFVRUNnd0xhVzVuY21WemN5MTBiSE13DQpIaGNOTWpFeE1USTNNVEkwTmpFMVdoY05NakV4TWpJM01USTBOakUxV2pBN01TTXdJUVlEVlFRRERCcGtZVzFwDQpZVzR1YzNSMVpHVnVkQzVzWVhOaGJHeGxMbU52YlRFVU1CSUdBMVVFQ2d3TGFXNW5jbVZ6Y3kxMGJITXdnZ0VpDQpNQTBHQ1NxR1NJYjNEUUVCQVFVQUE0SUJEd0F3Z2dFS0FvSUJBUUNaZDhJU21sRUpjdzZnTHFKUzAwTG4wWm95DQpuVTJGQjZ3TUZXbDVIREhEU1Y1M2NRN0tMUUF2OVRkVHUyNThwM1FDY21ZUi90ME81UjJ6K0xNTW1OSzhteVhLDQo0MzBVcHpNeGpIdi9LOWVxQ1dXMEhwTmJTUDhST3ZsWUNSRyswblJHcTJQUnN5b3VkSVBCVkpCRmlzMkk4TTI1DQo3elVOWVRGbm5uR2p4ZGROV2J3SmIwcktMbXJ5UVZnSEUvVVdWYnZXOGplMzRDVFpOWlVlWkRGOTlFRElIdVF6DQozMnpmWjA0ZVRkTEZMZzhYZjZ6NkI3TkhQVDZBSkpZcE9QV296amxRWi9vakhKUUx6cWVXc1hVemM5cXR5YXFODQorMTMxejJ6M0NGbkdDdzhSRXVrZzF3c1FBVmlhNi9OWnIxS0RSd3NGSmxyOU4vK09xOG9vTWN0eWVJTDFBZ01CDQpBQUV3RFFZSktvWklodmNOQVFFTEJRQURnZ0VCQUdMdU1nZDZlTk1HeTFHVjJQQzR2cEhNZHl1Z0M0RGo4RmpIDQpucGMra2tydmJrK2JNUWd4c0M4c3M4b0tseTNrQVg3a2ZoTDA2cSs2VkZVZkw0UmE2M2pKU1dQRGxqV2RhZFV5DQpLR05wUTRTeWthNEFQUVo2NXlxRHBmem9iU0VEdSt0amFhejZpL045R1pndUpMWHZOK1JhL2N0YXNLSzRBYm1rDQp2eStzeXlDKzR4Uk5xcUQ5QlBSQ1NnL2tQUFhOekU2c3JrbVJhdWJPa1RXbCtPQWZQbkpnVUtweGRpQ1BQN2sxDQpLN1Jsc3A3TDlsY0xHb1RZOThlZExBYjlWYXk3TmpRUXh6SmxJcFlRek5kZHpnYk02ZkxRSnVxdjB2c29MZGtlDQpYaGNpMWNQQVZJcDBwRmgxZ04zZW43Q0l6RnJqNi9ZUWxLWnZsazFKeEd1STZNNXRGTTQ9DQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0t
  tls.key: LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tDQpNSUlFdkFJQkFEQU5CZ2txaGtpRzl3MEJBUUVGQUFTQ0JLWXdnZ1NpQWdFQUFvSUJBUUNaZDhJU21sRUpjdzZnDQpMcUpTMDBMbjBab3luVTJGQjZ3TUZXbDVIREhEU1Y1M2NRN0tMUUF2OVRkVHUyNThwM1FDY21ZUi90ME81UjJ6DQorTE1NbU5LOG15WEs0MzBVcHpNeGpIdi9LOWVxQ1dXMEhwTmJTUDhST3ZsWUNSRyswblJHcTJQUnN5b3VkSVBCDQpWSkJGaXMySThNMjU3elVOWVRGbm5uR2p4ZGROV2J3SmIwcktMbXJ5UVZnSEUvVVdWYnZXOGplMzRDVFpOWlVlDQpaREY5OUVESUh1UXozMnpmWjA0ZVRkTEZMZzhYZjZ6NkI3TkhQVDZBSkpZcE9QV296amxRWi9vakhKUUx6cWVXDQpzWFV6YzlxdHlhcU4rMTMxejJ6M0NGbkdDdzhSRXVrZzF3c1FBVmlhNi9OWnIxS0RSd3NGSmxyOU4vK09xOG9vDQpNY3R5ZUlMMUFnTUJBQUVDZ2dFQUhZYkdCeEE3b1NiSzNKUzhWRlVqSFRqZGd2bHl6Ykp4TG8zOEVmb1R0Ty9yDQp4ZTB4bkVKV00rUG85aW9za3IwZys4dUl5ZE9RTW1wd2NZemEzZ1NWSHdaMm1mZDB2RUdDL3ZoNWJ0bS9qNDVwDQpTK05LRkprUndmM250VUZoY2FaS0NzSzAzTWFzeWUxTzRDWmw3NHRLQ2h2U2FRQ1Y0M0ljc0VQeXQwQzhNTHhUDQp4QTVPbndSUkpLUjJuaDEzNHZxUkd1T2NWTzl6OWEzaEFVNGlhazd5ZzRVUkhJL3NSRmNCQUtEK1A2dnJwVXFzDQpMSERQbGZWYTZ0UlQzV3hxTHZZZk1ScG5TM1krZEpLY3FZRmpJYXpsaHZrSEc1eTdFU25UWDFCajl2NjVRK3BWDQp5THVHWUtoUFZWUmxBUDFBUEZRaUwxM01JS1h1UXZKYXh0c2lhZVBaNFFLQmdRRElBaTNMcEpiMENiODZFTUFuDQprL0lQK3J3L1lzSVBHMk00Njh0UmpLKzNXL0RqZkxFYWlzMGxvN0hlQkdpbmg0L3dKQzBBSjN1WENPTlhBQzhZDQpNRGV1Z0VISHFieW9BR3NuNC8zNStRTm1jd3NEeStBa2VHa2pZR2xmdndoMVc2WmtsWFBOekp5QXNtYmt1aEhrDQphVDZ6YXdFbXpFZldvNUtoanJnZ3NWeGZhUUtCZ1FERWJqQzdwS0RtOE5jcExaSG8wL3BNR2lreDY3L2hud0ZPDQpsMGJXRVBRS0NFY0EweGpJbkRTSTdPbzUrUHlQclRUMkZtQ2VFeGF2ZTFTdXo3TW92eXpjdXgraG1FNHpBYzNtDQo0QlBEN3BFSlFYNk5wcGxmbEpGQ0NHZFNFdEFBQ3YwWjZYY2RHWVBvT0FnYkgxQzl3and2Zmw4SmR2WTdkUnJ6DQphL3BuRUZPaHJRS0JnR1ZJMVRBZk5XSjMzU0xtZ0tpS1B6YmdYcHdxd3VNcmwxK242TnB2U0R5dWpwcGs5cnJlDQpOMjBzTEZLbUlPbFZ1UXAvQjFyZ283R2tpcHZ3TitrajRjNjRuSmNqMmFtcUxKbVMxM2JUSjJtbU1KbkhrTVpuDQpRN2JxaC9JNHAydm04YWhXRnNMTkx5UHRORFg0RkQ5N0hJbDNVcW4zSVJvTTA2Q0V5YkdrbjRhaEFvR0FCanMwDQpvbzg3WjNnV2EwRDkwdlAraWJGK3d3Mm1mYUxNcU5DVnQ2NjcveUY2RnlXYk1Lbjc1S2dYV2hWeTlaL2FBVUIrDQplaVZDMy9nNm5IME1RdXVsb2E3eDl1OWM4N05IODk0QjRvSkw2WTVCVkg4SGVJZSsvK2hDVnRFdjlnQmpQUkl4DQo3Q05tRXRUTU5RZ09TS2JEd0FIelE5cVpQeE9jOGtIbnBLTlk4OVVDZ1lCT3oxMWJTdFh6WXQreVhCRlJqdWhyDQpFNzgzMjQ2N2FCMHFiODlIMkdTTzlkRFFlNUMrL3puUzFERFZ2VW5WV0lZcFhMaHhPWTRtZmVqN3M3SXNKSlNWDQpUZXNOeTdlUldOU2VkVHI5MFhMVXYwR09POUFxNFU0cG01SmNzcDJVanZrbWQzclJvbVBBQk00ZWRYd0twZXl1DQpLMGkwSGRwaWxXcGNEdUsyZ3k3SDVnPT0NCi0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS0=
type: kubernetes.io/tls
```

- Actualizo el ingress agregando:
```yaml
  tls:
  - hosts:
    - damian.student.lasalle.com
    secretName: ingress-tls
```

- Aplico los cambios del ingress:
```bash
kubectl apply -f ingress.yaml
```

- Nuevamente abro el tunnel de minikube
```bash
minikube tunnel
```

- Abro la pagina web
![](../imagenes/img2.png)
![](../imagenes/img3.png)
![](../imagenes/img4.png)
