- Creo el fichero hpa.yaml
```yaml
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: nginx-hpa
  namespace: nginx-server
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: nginx-server
  minReplicas: 1
  maxReplicas: 10
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 50
```
Este esta relacionado con el deployment del ejercicio 1.

- habilito el addons metrics-server de minikube
```bash
minikube addons enable metrics-server
```
- ejecuto el comando:
```bash
kubectl run -i --tty load-generator -n nginx-server --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://nginx-server; done"
```

- veo como escala
![](../imagenes/img7.png)