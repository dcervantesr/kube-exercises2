- Creo el fichero service-mongodb.yaml
```yaml
apiVersion: v1
kind: Service
metadata:
  name: mongodb
  labels:
    name: mongodb
spec:
  ports:
  - port: 27017
    targetPort: 27017
  clusterIP: None
  selector:
    app: mongodb
```

- Creo el fichero deployment-mongodb.yaml
```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mongodb
spec:
  serviceName: mongodb
  replicas: 3
  selector:
    matchLabels:
      app: mongodb
  template:
    metadata:
      labels:
        app: mongodb
        environment: mongodb
        replicaset: MainRepSet
    spec:
      containers:
        - name: mongodb
          image: mongo
          command:
            - "mongod"
            - "--bind_ip"
            - "0.0.0.0"
            - "--replSet"
            - "MainRepSet"
          ports:
            - containerPort: 27017
          volumeMounts:
            - name: mongodb
              mountPath: /data/db
  volumeClaimTemplates:
  - metadata:
      name: mongodb
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```

- Ejecuto los comandos:
```bash
kubectl apply -f service-mongodb.yaml
kubectl apply -f statefullset-mongodb.yaml
kubectl get pods
kubectl exec -it mongodb-0 -c mongodb bash
hostname -f // mongodb-0.mongodb.default.svc.cluster.local
mongosh
rs.initiate({ _id: "MainRepSet", version: 1, members: [ 
...  { _id: 0, host: "mongodb-0.mongodb.default.svc.cluster.local:27017" }, 
...  { _id: 1, host: "mongodb-1.mongodb.default.svc.cluster.local:27017" }, 
...  { _id: 2, host: "mongodb-2.mongodb.default.svc.cluster.local:27017" } ]});
use mynewdb
db.user.insert({name: "Damian"})
show dbs
```
![](../imagenes/img5.png)

```bash
exit
exit
kubectl exec -it mongodb-2 -c mongodb bash
mongosh
show dbs
```
![](../imagenes/img6.png)



## Diferencias que existiría si el montaje se hubiera realizado con el objeto de ReplicaSet
De haber utilizado un ReplicaSet los nombres se generan con un hash aleatorio.